package simplePackage;


/**
 * Created by Arman on 29-Apr-16.
 */
public abstract class Card implements Comparable<Card>
{
    private final Suit suit;
    private final Rank rank;
    private final Color color;

    public Card(Suit s, Color c, Rank r){
        suit = s;
        color = c;
        rank = r;
    }

    public Suit getSuit(){
        return suit;
    }

    public Color getColor(){
        return color;
    }

    public Rank getRank(){
        return rank;
    }

    public abstract int getWeight();

    public abstract int getWeightT();

    @Override
    public int compareTo(Card o) {

        if(suit == o.suit && rank == o.rank && color == o.color){
            return 0;
        }

        if(suit.ordinal() > o.suit.ordinal()) {
            return 1;
        } else if(suit.ordinal() == o.suit.ordinal()){
            return rank.ordinal() > rank.ordinal()? 1 : -1;
        }
        else {
            return -1;
        }
    }

    /*public ISFSObject toSFSObject() {
        SFSObject sfsObject = SFSObject.newInstance();
        sfsObject.putInt("s", this.suit.getValue());
        sfsObject.putInt("r", this.rank.getValue());
        sfsObject.putInt("c", this.color.ordinal());

        return sfsObject;
    }*/
}
