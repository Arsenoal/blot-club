package simplePackage;
/*
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Arman on 21/06/2016.
 */
public class PlayerCards {

    private List<BBCard> clubs;
    private List<BBCard> diamonds;
    private List<BBCard> hearts;
    private List<BBCard> spades;



    public PlayerCards() {
        clubs = new ArrayList<>();
        diamonds = new ArrayList<>();
        hearts = new ArrayList<>();
        spades = new ArrayList<>();
    }

    public PlayerCards(List<BBCard> list) {

        this();

        list.forEach(card -> {
            BBCard bbCard = (BBCard)card;
            bbCard.setTrumpType(BBTrump.TrumpType.NULL);
            addCard(bbCard);
        });
    }

    public void addCard(BBCard card) {

        switch (card.getSuit()) {
            case CLUBS:
                clubs.add(card);
                break;
            case DIAMONDS:
                diamonds.add(card);
                break;
            case HEARTS:
                hearts.add(card);
                break;
            case SPADES:
                spades.add(card);
                break;
        }
    }

    public void sort() {

        Collections.sort(clubs, BBCard::compareTo);
        Collections.sort(diamonds, BBCard::compareTo);
        Collections.sort(hearts, BBCard::compareTo);
        Collections.sort(spades, BBCard::compareTo);
    }

    public List<BBCard> getList() {
        List<BBCard> list = new ArrayList<>(clubs);
        list.addAll(diamonds);
        list.addAll(hearts);
        list.addAll(spades);
        return list;
    }

    public void clear(){
        clubs.clear();
        diamonds.clear();
        hearts.clear();
        spades.clear();
    }

    /*public ISFSObject toSFSObject(){

        return SFSObject.newInstance();
    }*/
}
