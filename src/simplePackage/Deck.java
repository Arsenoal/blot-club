package simplePackage;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Arman on 29-Apr-16.
 */
public abstract class Deck {

    protected List<Card> deck;

    public boolean addCard(Card card) {

        return deck.add(card);
    }

    public void addCard(int index, Card card) {

        deck.add(index, card);
    }

    public boolean addCards(Collection<Card> cards) {

        return deck.addAll(cards);
    }

    public boolean addCards(int index, Collection<Card> cards) {

        return deck.addAll(index, cards);
    }

    public Card removeCard(int index) {

        return deck.remove(index);
    }

    public boolean removeCard(Card card) {

        return deck.remove(card);
    }

    public Card getCard(int index) {

        return deck.get(index);
    }

    public int size() {
        return deck.size();
    }

    public List<Card> subList(int from, int to) {
        return deck.subList(from, to);
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public void shuffle(Random rnd) {
        Collections.shuffle(deck, rnd);
    }
}