package simplePackage;

import java.util.*;

public class CardSort {
    private ArrayList<BBCard> cards = new ArrayList<>();
    private ArrayList<BBCard> clubs = new ArrayList<>();
    private ArrayList<BBCard> diamonds = new ArrayList<>();
    private ArrayList<BBCard> hearts = new ArrayList<>();
    private ArrayList<BBCard> spades = new ArrayList<>();

    private List<ArrayList<BBCard>> listOfPlayerCards = new ArrayList<>();

    BBTrump trump;

    public CardSort(BBTrump trump, List<BBCard> cards) {
        this.cards = new ArrayList<>(cards);
        this.trump = trump;

        for (BBCard card: cards) {
            if (card.getSuit() == Suit.CLUBS){ clubs.add(card); }
            if (card.getSuit() == Suit.DIAMONDS){ diamonds.add(card); }
            if (card.getSuit() == Suit.HEARTS){ hearts.add(card); }
            if (card.getSuit() == Suit.SPADES){ spades.add(card); }
        }

        this.cards.clear();
        this.cards.addAll(clubs);
        this.cards.addAll(diamonds);
        this.cards.addAll(hearts);
        this.cards.addAll(spades);

        listOfPlayerCards.add(clubs);
        listOfPlayerCards.add(diamonds);
        listOfPlayerCards.add(hearts);
        listOfPlayerCards.add(spades);
    }

    /**
     * sorting cards by their by descending value.
     */
    public void sortCardsByValue(){
        cards.clear();

        if(trump == BBTrump.NO_TRUMP) //no trump case
        {
            for(ArrayList<BBCard> tmpList: listOfPlayerCards){
                for(int i = 0; i < tmpList.size(); i++){
                    for(int j = 0; j < tmpList.size(); j++){
                        if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight()){
                            BBCard tmpCard = tmpList.get(i);
                            tmpList.set(i, tmpList.get(j));
                            tmpList.set(j, tmpCard);
                        }
                    }
                }
                cards.addAll(tmpList);
            }
        }
        if(trump.getSuit() == Suit.CLUBS) //Clubs trump case
        {
            for(ArrayList<BBCard> tmpList: listOfPlayerCards){
                for(int i = 0; i < tmpList.size(); i++){
                    for(int j = 0; j < tmpList.size(); j++){
                        if(tmpList.get(i).getSuit() == Suit.CLUBS){
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }else{
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }
                    }
                }
                cards.addAll(tmpList);
            }
        }
        if(trump.getSuit() == Suit.DIAMONDS) //Diamonds trump case
        {
            for(ArrayList<BBCard> tmpList: listOfPlayerCards){
                for(int i = 0; i < tmpList.size(); i++){
                    for(int j = 0; j < tmpList.size(); j++){
                        if(tmpList.get(i).getSuit() == Suit.DIAMONDS){
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }else{
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }
                    }
                }
                cards.addAll(tmpList);
            }
        }
        if(trump.getSuit() == Suit.HEARTS) //Hearts trump case
        {
            for(ArrayList<BBCard> tmpList: listOfPlayerCards){
                for(int i = 0; i < tmpList.size(); i++){
                    for(int j = 0; j < tmpList.size(); j++){
                        if(tmpList.get(i).getSuit() == Suit.HEARTS){
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }else{
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }
                    }
                }
                cards.addAll(tmpList);
            }
        }
        if(trump.getSuit() == Suit.SPADES) //Spdaes trump case
        {
            for(ArrayList<BBCard> tmpList: listOfPlayerCards){
                for(int i = 0; i < tmpList.size(); i++){
                    for(int j = 0; j < tmpList.size(); j++){
                        if(tmpList.get(i).getSuit() == Suit.SPADES){
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }else{
                            if(tmpList.get(i).getWeight(trump) > tmpList.get(j).getWeight(trump)){
                                BBCard tmpCard = tmpList.get(i);
                                tmpList.set(i, tmpList.get(j));
                                tmpList.set(j, tmpCard);
                            }
                        }
                    }
                }
                cards.addAll(tmpList);
            }
        }
    }

    public List<BBCard> getCards(){
        return this.cards;
    }
    public String toString(){
        String cardStr = "";

        for(BBCard card: this.clubs){
            cardStr += card.toString();
        }
        for(BBCard card: this.diamonds){
            cardStr += card.toString();
        }
        for(BBCard card: this.hearts){
            cardStr += card.toString();
        }
        for(BBCard card: this.spades){
            cardStr += card.toString();
        }

        return cardStr;
    }
}
