package simplePackage;

import java.util.*;

/**
 * Created by Arsen on 7/1/2016.
 */
public class Main {

    public static void main(String[] args) {
        Vector<Pair<Player, BBCard>> checkCards = new Vector<>();
        PlayerCards playerCards = new PlayerCards();

        playerCards.addCard(new BBCard(Suit.SPADES, Rank.EIGHT));
        playerCards.addCard(new BBCard(Suit.SPADES, Rank.TEN));
        playerCards.addCard(new BBCard(Suit.HEARTS, Rank.JACK));
        playerCards.addCard(new BBCard(Suit.HEARTS, Rank.FOUR));
        playerCards.addCard(new BBCard(Suit.SPADES, Rank.QUEEN));
        playerCards.addCard(new BBCard(Suit.CLUBS, Rank.SEVEN));
        playerCards.addCard(new BBCard(Suit.CLUBS, Rank.EIGHT));
        playerCards.addCard(new BBCard(Suit.CLUBS, Rank.NINE));

        checkCards.add(new Pair<>(Player.PLAYER_1, new BBCard(Suit.DIAMONDS, Rank.KING)));
        checkCards.add(new Pair<>(Player.PLAYER_2, new BBCard(Suit.CLUBS, Rank.QUEEN)));
        checkCards.add(new Pair<>(Player.PLAYER_2, new BBCard(Suit.CLUBS, Rank.TEN)));

        Main sample = new Main();

        sample.getAllowedCards(checkCards, playerCards, BBTrump.NO_TRUMP);

        for(BBCard card: playerCards.getList()){
            System.out.println(card.getSuit() + " " + card.getRank() + ": " + card.getAllowed());
        }
    }


// @params Params (Vector(Pair(player, curPlayer)), Trump, Pair(Player, Card))
// @return Boolean

   /* public boolean checkCards(Vector<Pair<Player, Card>> checkVector, Pair<Player, Card> checkPair, Trump checkTrump) {

        boolean answer = true;
        Suit checkSuit = checkTrump.getSuit();
        boolean noTrump = checkSuit == Suit.NO_SUIT;
        Player checkPlayer = (Player) checkPair.first;
        Card checkCard = (Card) checkPair.second;

        Iterator i = checkVector.iterator();

        if (checkVector.isEmpty()) answer = true;
        else {
            Pair firstPair = checkVector.firstElement();
            Player firstPlayer = (Player) firstPair.first;
            Card firstCard = (Card) firstPair.second;
            int mathAbs = Math.abs(firstPlayer.getValue() - checkPlayer.getValue());
            boolean firstIsTeamMate = mathAbs == 2;

            if (firstIsTeamMate) {
                if (checkCard.getSuit() == firstCard.getSuit()) {
                    if (checkCard.getSuit() == checkTrump.getSuit() && firstCard.getWeight() < checkCard.getWeight()) {

                    }
                }
            }

            while (i.hasNext()) {
//                if (iteration == 0)
                Pair curPair = (Pair) i.next();
                Player curPlayer = (Player) curPair.first;
                Card curCard = (Card) curPair.second;
                Suit curSuit = curCard.getSuit();

            }
        }


        return answer;
    }*/

    /**
     * @param checkCards - cards that have already been in current hand.
     * @param checkPlayerCards - cards that current player has.
     * @param checkTrump - current Trump.
     * @return cards that allowed to play.
     */
// @params (Vector(Player, Card), PlayerCards, Trump)
// $return PlayerCards
    public PlayerCards getAllowedCards(Vector<Pair<Player, BBCard>> checkCards, PlayerCards checkPlayerCards, BBTrump checkTrump){
        boolean isFriendWinner = true;
        PlayerCards playerAllowedCards = new PlayerCards();

        BBCard winnerCard = new BBCard(Suit.NO_SUIT, Rank.SEVEN);

        for(Pair<Player, BBCard> pair: checkCards){
            if(pair.second.getSuit() == checkTrump.getSuit() && winnerCard.getSuit() == checkTrump.getSuit() &&
                    pair.second.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)){
                winnerCard = pair.second;
            }
            if(pair.second.getSuit() == checkTrump.getSuit() && winnerCard.getSuit() != checkTrump.getSuit()){
                winnerCard = pair.second;
            }
            if(pair.second.getSuit() != checkTrump.getSuit() && winnerCard.getSuit() != checkTrump.getSuit() &&
                    pair.second.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)){
                winnerCard = pair.second;
            }
            if(winnerCard.getSuit() == Suit.NO_SUIT && pair.second.getSuit() != checkTrump.getSuit() &&
                    pair.second.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)){
                winnerCard = pair.second;
            }
        }
        for(BBCard card: checkPlayerCards.getList()){

            if(checkCards.size() != 0){
                BBCard firstCard = checkCards.get(0).second;

                if(card.getSuit() == firstCard.getSuit() && firstCard.getSuit() != checkTrump.getSuit()){

                    playerAllowedCards.addCard(card);
                }
                if(card.getSuit() == firstCard.getSuit() && firstCard.getSuit() == checkTrump.getSuit() &&
                        card.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)){

                    playerAllowedCards.addCard(card);
                }
            }

        }

        for(BBCard playerCard: checkPlayerCards.getList()){

            if(checkCards.size() != 0){ //checking if our player is first player.
                BBCard firstCard = checkCards.get(0).second;

                if(playerCard.getSuit() != firstCard.getSuit() && playerAllowedCards.getList().size() == 0){
                    if(firstCard.getSuit() != checkTrump.getSuit() && winnerCard.getSuit() == checkTrump.getSuit() &&
                            playerCard.getSuit() == checkTrump.getSuit()){

                        if(playerCard.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)) {
                                playerAllowedCards.addCard(playerCard);
                        }
                    }
                    if(firstCard.getSuit() != checkTrump.getSuit() && winnerCard.getSuit() != checkTrump.getSuit() &&
                            playerCard.getSuit() == checkTrump.getSuit()){

                        playerAllowedCards.addCard(playerCard);
                    }
                }
                if(checkTrump == BBTrump.NO_TRUMP && playerAllowedCards.getList().size() == 0){
                    if(playerCard.getWeight(checkTrump) > winnerCard.getWeight(checkTrump)){
                        playerAllowedCards.addCard(playerCard);
                    }
                }
            }
        }

        if(playerAllowedCards.getList().size() == 0 || checkCards.size() == 0 || !isFriendWinner){
            playerAllowedCards.clear();
            playerAllowedCards = checkPlayerCards;
        }
        for(BBCard card: checkPlayerCards.getList()){ card.setAllowed(false); }
        for(BBCard allowedCard: playerAllowedCards.getList()){ allowedCard.setAllowed(true); }

        return playerAllowedCards;
    }
}
