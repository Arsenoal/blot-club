package simplePackage;


import java.util.*;

/**
 * Created by Arsen on 6/27/2016.
 */
public class Combo {
    private BBTrump trump;
    private List<Integer> fourPaperCardNumber = new ArrayList<>();
    private List<BBCard> playerCards;

    private List<ArrayList<BBCard>> listOfPlayerCards = new ArrayList<>();

    Combo(List<BBCard> playerCards, BBTrump trump){
        this.playerCards = new ArrayList<>(playerCards);
        this.trump = trump;

        ArrayList<BBCard> clubs = new ArrayList<>();
        ArrayList<BBCard> diamonds = new ArrayList<>();
        ArrayList<BBCard> hearts = new ArrayList<>();
        ArrayList<BBCard> spades = new ArrayList<>();

        for (BBCard card: playerCards) {
            if (card.getSuit() == Suit.CLUBS){ clubs.add(card); }
            if (card.getSuit() == Suit.DIAMONDS){ diamonds.add(card); }
            if (card.getSuit() == Suit.HEARTS){ hearts.add(card); }
            if (card.getSuit() == Suit.SPADES){ spades.add(card); }
        }

        listOfPlayerCards.add(clubs);
        listOfPlayerCards.add(diamonds);
        listOfPlayerCards.add(hearts);
        listOfPlayerCards.add(spades);

    }

    private void sortCardsByNumber(){
        playerCards.clear();

        for(ArrayList<BBCard> tmpList: listOfPlayerCards){
            for(int i = 0; i < tmpList.size(); i++){
                for(int j = 0; j < tmpList.size(); j++){
                    if(tmpList.get(i).getRank().getValue() > tmpList.get(j).getRank().getValue()){
                        BBCard tmpCard = tmpList.get(i);
                        tmpList.set(i, tmpList.get(j));
                        tmpList.set(j, tmpCard);
                    }
                }
            }
            playerCards.addAll(tmpList);
        }
    }

    /**
     *
     * @return - returns each player's combination that could be claimed.
     */
    int getCombinationValue(){
        int teamValue = 0;
        int quantity;

        this.sortCardsByNumber();

        fourPapers(playerCards);

        if(fourPaperCardNumber.size() != 0 && trump.getSuit() == Suit.NO_SUIT){
            for(int a: fourPaperCardNumber){
                if(a == Rank.ACE.getValue()){ teamValue += 190; }
                if(a == Rank.TEN.getValue() || a == Rank.JACK.getValue() ||
                        a == Rank.QUEEN.getValue() || a == Rank.KING.getValue()){ teamValue += 100; }
            }
        }else if(fourPaperCardNumber.size() != 0 && trump.getSuit() != Suit.NO_SUIT){
            for(int a: fourPaperCardNumber) {
                if (a == Rank.JACK.getValue()) { teamValue += 200; }
                if (a == Rank.NINE.getValue()) { teamValue += 140; }
                if (a == Rank.ACE.getValue()) { teamValue += 110; }
                if (a == Rank.TEN.getValue() || a == Rank.QUEEN.getValue() || a == Rank.KING.getValue()) { teamValue += 100; }
            }
        }

        if(hundred(playerCards)){ teamValue += 100; }
        if((quantity = fifty(playerCards)) != 0){ teamValue += quantity*50; }
        if((quantity = terz(playerCards)) != 0){ teamValue += quantity*20; }
        if(reBlot(playerCards)){ teamValue += 20; }

        return teamValue;
    }

    private int terz(List<BBCard> playersCards) {
        int counter = 1;
        int quantityCounter = 0;
        List<BBCard> tempList = new ArrayList<>();

        for(int i = 0, size = playersCards.size(); i < size; i++){
            if(i != size - 1 && playersCards.get(i).getSuit() == playersCards.get(i + 1).getSuit() &&
                    playersCards.get(i).getRank().getValue() - 1 == playersCards.get(i + 1).getRank().getValue()){

                counter++;
            }else{
                if(counter == 3 || counter == 8){ quantityCounter++; }
                counter = 1;
            }
        }

        return quantityCounter;
    }
    private int fifty(List<BBCard> playersCards){
        int counter = 1;
        int quantityCounter = 0;

        for(int i = 0, size = playersCards.size(); i < size; i++){
            if(i != size - 1 && playersCards.get(i).getSuit() == playersCards.get(i + 1).getSuit() &&
                    playersCards.get(i).getRank().getValue() - 1 == playersCards.get(i + 1).getRank().getValue()){

                counter++;
            }else{
                if(counter == 4){ quantityCounter++; }
                counter = 1;
            }
        }

        return quantityCounter;
    }
    private boolean hundred(List<BBCard> playersCards){
        boolean m = false;
        int counter = 1;
        int tmpCounter = 0;

        for(int i = 0; i < playersCards.size() - 1; i++){
            if((playersCards.get(i).getSuit() == playersCards.get(i + 1).getSuit()) &&
                    playersCards.get(i).getRank().getValue() - 1 == playersCards.get(i + 1).getRank().getValue()){
                counter++;

                if(tmpCounter < counter){ tmpCounter = counter; }
            }else{ counter = 1; }
        }

        if(tmpCounter == 5 || tmpCounter == 6 || tmpCounter == 7 || tmpCounter == 8){ return !m; }

        return m;
    }
    private void fourPapers(List<BBCard> playersCards){
        List<BBCard> tmpList = new ArrayList<>();

        BBCard tmpCard;
        int counter = 0;

        for(int i = 0; i < playersCards.size(); i++){
            tmpCard = playersCards.get(i);

            for(int j = 0; j < playersCards.size(); j++){
                if(tmpCard.getRank() == playersCards.get(j).getRank() && (tmpCard.getRank().getValue() != 7 &&
                        tmpCard.getRank().getValue() != 8 && tmpCard.getRank().getValue() != 9)){
                    tmpList.add(playersCards.get(j));

                    counter++;
                }
                if(counter == 4){
                    this.fourPaperCardNumber.add(tmpCard.getRank().getValue());
                    playersCards.removeAll(tmpList);
                }
            }
            counter = 0;
        }
    }
    private boolean reBlot(List<BBCard> playerCards){
        boolean m = false;
        //List<BBCard> tmpList = new ArrayList<>();

        int counter = 0;

        for(BBCard card: playerCards){
            if((card.getSuit() == trump.getSuit()) && ((card.getRank().getValue() == 12) || (card.getRank().getValue() == 13))){
                //tmpList.add(card);
                counter++;
            }
        }

        if(counter == 2){
            return !m;
        }

        return m;
    }
}
