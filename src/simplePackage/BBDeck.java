package simplePackage;

import java.util.ArrayList;

/**
 * Created by Arman on 07-Jun-16.
 */
public class BBDeck extends Deck {

    public BBDeck() {
        deck = new ArrayList<>(32);

        //Clubs
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.SEVEN));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.EIGHT));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.NINE));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.TEN));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.JACK));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.QUEEN));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.KING));
        addCard(new BBCard(Suit.CLUBS, Color.BLACK, Rank.ACE));

        //Diamonds
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.SEVEN));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.EIGHT));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.NINE));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.TEN));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.JACK));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.QUEEN));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.KING));
        addCard(new BBCard(Suit.DIAMONDS, Color.RED, Rank.ACE));

        //Hearts
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.SEVEN));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.EIGHT));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.NINE));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.TEN));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.JACK));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.QUEEN));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.KING));
        addCard(new BBCard(Suit.HEARTS, Color.RED, Rank.ACE));

        //Spades
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.SEVEN));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.EIGHT));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.NINE));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.TEN));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.JACK));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.QUEEN));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.KING));
        addCard(new BBCard(Suit.SPADES, Color.BLACK, Rank.ACE));
    }
}
