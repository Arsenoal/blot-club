package simplePackage;

/**
 * Created by Arman on 29-Apr-16.
 */
public enum Suit {

    NO_SUIT(0), CLUBS(1), DIAMONDS(2), HEARTS(3), SPADES(4);;

    private int value;

    Suit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Suit valueOf(int value) {
        Suit result = Suit.SPADES;
        switch (value) {
            case 0:
                result = Suit.NO_SUIT;
                break;
            case 1:
                result = Suit.CLUBS;
                break;
            case 2:
                result = Suit.DIAMONDS;
                break;
            case 3:
                result = Suit.HEARTS;
                break;
            case 4:
                result = Suit.SPADES;
                break;
        }
        return result;
    }
}
