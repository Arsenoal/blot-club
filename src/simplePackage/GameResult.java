package simplePackage;

import java.util.*;

class GameResult {

    private BBTrump trump;
    private Player claimedPlayer;

    private int firstTeamGainedValue = 0;
    private int secondTeamGainedValue = 0;

    GameResult(Pair<Player, BBTrump> claimedPlayer, Vector<Pair<Player, BBCard>> firstTeamsFinalGainedCards, Vector<Pair<Player, BBCard>> secondTeamsFinalGainedCards){
        this.trump = claimedPlayer.second;
        this.claimedPlayer = claimedPlayer.first;

        for(Pair<Player, BBCard> currentPair: firstTeamsFinalGainedCards){
            firstTeamGainedValue += currentPair.second.getWeight(trump);
        }
        for(Pair<Player, BBCard> currentPair: secondTeamsFinalGainedCards){
            secondTeamGainedValue += currentPair.second.getWeight(trump);
        }
    }

    private int getFirstTeamGainedValue(){
        return firstTeamGainedValue;
    }
    private int getSecondTeamGainedValue(){
        return secondTeamGainedValue;
    }

    /**
     *
     * @param initialPlayersCards - checking each player's announced combination.
     * @return - returns a list of Team and Teams gained values.
     */
    Vector<Pair<Team, Integer>> currentWinnerTeam(Vector<PlayerCards> initialPlayersCards, int lastHandPointer){
        int pointer = 0;
        int claimedValue = trump.getValue();

        Vector<Pair<Team, Integer>> tempList = new Vector<>();

        Combo firstPlayerCombo = new Combo(initialPlayersCards.get(0).getList(), trump);
        Combo secondPlayerCombo = new Combo(initialPlayersCards.get(1).getList(), trump);
        Combo thirdPlayerCombo = new Combo(initialPlayersCards.get(2).getList(), trump);
        Combo fourthPlayerCombo = new Combo(initialPlayersCards.get(3).getList(), trump);

        int firstTeamCombinationValue = firstPlayerCombo.getCombinationValue() + thirdPlayerCombo.getCombinationValue();
        int secondTeamCombinationValue = secondPlayerCombo.getCombinationValue() + fourthPlayerCombo.getCombinationValue();

        int firstTeamGainedFinalValue = firstTeamCombinationValue + getFirstTeamGainedValue();
        int secondTeamGainedFinalValue = secondTeamCombinationValue + getSecondTeamGainedValue();

        if(lastHandPointer == 1){ firstTeamGainedFinalValue += 10; }
        if(lastHandPointer == 2){ secondTeamGainedFinalValue += 10; }
        if(lastHandPointer != 1 && lastHandPointer != 2){
            System.out.println("Incorrect last hand input message");
            System.exit(1);
        }

        if(claimedPlayer == Player.PLAYER_1 || claimedPlayer == Player.PLAYER_3){ pointer = 1; }
        if(claimedPlayer == Player.PLAYER_2 || claimedPlayer == Player.PLAYER_4){ pointer = 2; }

        if(pointer == 1){
            if(firstTeamGainedFinalValue < claimedValue*10){
                if(firstTeamGainedFinalValue == 0){
                    secondTeamGainedFinalValue += claimedValue*10 + 90;
                }else{
                    firstTeamGainedFinalValue = 0;
                    secondTeamGainedFinalValue += claimedValue*10 + 160;
                }
                if(trump.getContra()){ secondTeamGainedFinalValue += getSecondTeamGainedValue(); }
                if(trump.getReContra()){ secondTeamGainedFinalValue += 3*getSecondTeamGainedValue(); }
            }
            if(firstTeamGainedFinalValue >= claimedValue*10){
                firstTeamGainedFinalValue += claimedValue*10;

                if(secondTeamGainedFinalValue == 0){ firstTeamGainedFinalValue += 90; }
                if(trump.getContra()){ firstTeamGainedFinalValue += getFirstTeamGainedValue(); }
                if(trump.getReContra()){ firstTeamGainedFinalValue += 3*getFirstTeamGainedValue(); }
            }
        }
        if(pointer == 2) {
            if(secondTeamGainedFinalValue < claimedValue*10){
                if(secondTeamGainedFinalValue == 0){
                    firstTeamGainedFinalValue += claimedValue*10 + 90;
                }else{
                    secondTeamGainedFinalValue = 0;
                    firstTeamGainedFinalValue += claimedValue*10 + 160;
                }
                if(trump.getContra()){ firstTeamGainedFinalValue += getFirstTeamGainedValue(); }
                if(trump.getReContra()){ firstTeamGainedFinalValue += 3*getFirstTeamGainedValue(); }
            }
            if(secondTeamGainedFinalValue >= claimedValue*10){
                secondTeamGainedFinalValue += claimedValue*10;

                if(firstTeamGainedFinalValue == 0){ secondTeamGainedFinalValue += 90; }
                if(trump.getContra()){ secondTeamGainedFinalValue += getSecondTeamGainedValue(); }
                if(trump.getReContra()){ secondTeamGainedFinalValue += 3*getSecondTeamGainedValue(); }
            }
        }

        tempList.add(new Pair<>(Team.TEAM_ODD, firstTeamGainedFinalValue));
        tempList.add(new Pair<>(Team.TEAM_EVEN, secondTeamGainedFinalValue));

        return tempList;
    }

    /**
     *
     * @param currentCards - cards that are already in a table.
     * @return - Player who won and value that he won.
     */
    Pair<Player, Integer> getHandResult(Vector<Pair<Player, BBCard>> currentCards){

        Player winner = this.getWinner(currentCards);
        this.currentHandScore(currentCards);

        return new Pair<>(winner, firstTeamGainedValue + secondTeamGainedValue);
    }

    private Player getWinner(Vector<Pair<Player, BBCard>> currentCards){
        Player winner = Player.PLAYER_1;
        BBCard winnerCard = currentCards.get(0).second;

        for(Pair<Player, BBCard> currentPair: currentCards){
            BBCard currentCard = currentPair.second;

            if (winnerCard.getSuit() == trump.getSuit() && currentCard.getSuit() == trump.getSuit() ||
                    winnerCard.getSuit() != trump.getSuit() && currentCard.getSuit() != trump.getSuit()){
                if(winnerCard.getWeight(trump) < currentCard.getWeight(trump)){
                    winnerCard = currentCard;
                }
            }
            if (winnerCard.getSuit() != trump.getSuit() && currentCard.getSuit() == trump.getSuit()) { winnerCard = currentCard; }
        }

        for(Pair<Player, BBCard> currentPair: currentCards){
            BBCard currentCard = currentPair.second;

            if(currentCard == winnerCard){ winner = currentPair.first; }
        }

        return winner;
    } // just supporting method
    private void currentHandScore(Vector<Pair<Player, BBCard>> currentCards) {
        List<BBCard> currentHandCards = new ArrayList<>();

        for(Pair<Player, BBCard> currentPair: currentCards){ currentHandCards.add(currentPair.second); }

        if(this.getWinner(currentCards).getValue() == 1 || this.getWinner(currentCards).getValue() == 3){
            for (BBCard card : currentHandCards) { //counting first team gained value
                firstTeamGainedValue += card.getWeight(trump);
            }
        }
        if(this.getWinner(currentCards).getValue() == 2 || this.getWinner(currentCards).getValue() == 4){
            for (BBCard card : currentHandCards) { //counting second team gained value
                secondTeamGainedValue += card.getWeight(trump);
            }
        }
    } // just supporting method

}